-module(fizzbuzz).
-export([fizzbuzz/1, check/1]).

fizzbuzz([]) -> ok;
fizzbuzz([H|T]) ->
	check(H),
	fizzbuzz(T).

check(H) when (H rem 3 =:=0 ) and (H rem 5 =:= 0) -> io:fwrite("fizzbuzz\n");
check(H) when (H rem 3 =:= 0) -> io:fwrite("buzz\n");
check(H) when (H rem 5 =:= 0) -> io:fwrite("fizz\n");
check(H) -> io:fwrite("~w\n", [H]).
